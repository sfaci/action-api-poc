# Action API PoC

This is an Action API PoC (currently with sample code written in Python and Go) just to see how MediaWiki Action API works. 

## go

You will need to run locally a MediaWiki instance and type the right username and password in the _doLogin_ function in the _main.go_ file 

## python

In this case you need to run locally a MediaWiki as well and type the right username and password in the proper place (when login request is performed)
