package main

import (
	"action_api/entities"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"strings"
)

// Global client to keep the same session (in the first call the session cookie is set for the later calls)
var client = http.Client{}

const APIUrl = "http://localhost:8080/w/api.php"

func main() {
	loginToken := getLoginToken()
	fmt.Println("logintoken: ", loginToken)

	csrfToken := doLogin(loginToken)
	fmt.Println("CSRF Token: ", csrfToken)

	createPage(csrfToken, "New page", "This is a new page", "This is the summary")
	createPage(csrfToken, "New page2", "This is a new page2", "This is the summary2")
}

// GET a login token
func getLoginToken() string {
	urlBase, _ := url.Parse(APIUrl)
	parameters := url.Values{}
	parameters.Add("action", "query")
	parameters.Add("meta", "tokens")
	parameters.Add("type", "login")
	parameters.Add("format", "json")
	urlBase.RawQuery = parameters.Encode()
	response, error := client.Get(urlBase.String())
	if error != nil {
		fmt.Print(error.Error())
		os.Exit(1)
	}

	// Reuse the cookie from the previous call to keep the session
	cookies := response.Cookies()
	urlB, _ := url.Parse(APIUrl)
	jar, _ := cookiejar.New(nil)
	jar.SetCookies(urlB, cookies)
	client = http.Client{Jar: jar}

	responseData, error := io.ReadAll(response.Body)
	if error != nil {
		log.Fatal(error)
	}
	var tokenResponse entities.TokenResponse
	json.Unmarshal(responseData, &tokenResponse)

	return tokenResponse.Query.Tokens.LoginToken
}

/*
* Do login, get a CSRF token and return it
 */
func doLogin(loginToken string) string {
	// Do login
	formData := url.Values{}
	formData.Add("action", "clientlogin")
	formData.Add("username", "Test2")
	formData.Add("password", "wikimediatest")
	formData.Add("loginreturnurl", "http://localhost:5000")
	formData.Add("logintoken", loginToken)
	formData.Add("format", "json")

	request, _ := http.NewRequest("POST", APIUrl, strings.NewReader(formData.Encode()))
	request.Header.Set("Content-Type", "multipart/form-data")

	response, error := client.PostForm(APIUrl, formData)
	if error != nil {
		fmt.Print(error.Error())
		os.Exit(1)
	}

	responseData, error := io.ReadAll(response.Body)
	if error != nil {
		log.Fatal(error)
	}

	fmt.Println("[DEBUG] Login: ", string(responseData))

	// Get a CSRF token
	formData = url.Values{}
	formData.Add("action", "query")
	formData.Add("meta", "tokens")
	formData.Add("type", "csrf")
	formData.Add("format", "json")
	response, error = client.PostForm(APIUrl, formData)
	if error != nil {
		fmt.Print(error.Error())
		os.Exit(1)
	}

	responseData, error = io.ReadAll(response.Body)
	if error != nil {
		log.Fatal(error)
	}

	var csrfResponse entities.CSRFResponse
	json.Unmarshal(responseData, &csrfResponse)
	return csrfResponse.Query.Tokens.CSRFToken
}

// Create a page with the information provided as parameters
func createPage(csrfToken string, title string, text string, summary string) {
	formData := url.Values{}
	formData.Add("token", csrfToken)
	formData.Add("action", "edit")
	formData.Add("title", title)
	formData.Add("text", text)
	formData.Add("summary", summary)
	formData.Add("format", "json")

	request, _ := http.NewRequest("POST", APIUrl, strings.NewReader(formData.Encode()))
	request.Header.Set("Content-Type", "multipart/form-data")

	response, _ := client.PostForm(APIUrl, formData)
	responseData, _ := io.ReadAll(response.Body)
	fmt.Println("[DEBUG] Create page: ", string(responseData))
}
