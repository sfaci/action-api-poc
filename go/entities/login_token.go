package entities

type TokenResponse struct {
	BatchComplete string `json:"batchcomplete"`
	Query         Query
}

type Query struct {
	Tokens Token
}

type Token struct {
	LoginToken string `json:"logintoken"`
}
