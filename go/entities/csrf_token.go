package entities

type CSRFResponse struct {
	BatchComplete string `json:"batchcomplete"`
	Query         CSRFQuery
}

type CSRFQuery struct {
	Tokens CSRFToken
}

type CSRFToken struct {
	CSRFToken string `json:"csrftoken"`
}
