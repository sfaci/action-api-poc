import requests

session = requests.Session()
URL = "http://localhost:8080/w/api.php"

'''
Login action
'''
# Get a logintoken
token_response = session.get(
        url=URL,
        params={
            'action': "query",
            'meta': "tokens",
            'type': "login",
            'format': "json"})
token_data = token_response.json()
print("Login token: " + str(token_data))
login_token = token_data['query']['tokens']['logintoken']

# Do login
login_response = session.post(url=URL, data={
    'action': "clientlogin",
    'username': "Test2",
    'password': "wikimediatest",
    'loginreturnurl': 'http://localhost:5000/',
    'logintoken': login_token,
    'format': "json"
})
login_data = login_response.json()
print("Login: " + str(login_data))

'''
Create page action
'''
# Get a crsf token
csrf_token_response = session.get(
        url=URL,
        params={
            'action': "query",
            'meta': "tokens",
            'type': "csrf",
            'format': "json"})
csrf_token_data = csrf_token_response.json()
print("CSRF token: " + str(csrf_token_data))
csrf_token = csrf_token_data['query']['tokens']['csrftoken']

# Create the new page
create_page_response = session.post(url=URL, data={
    "action": "edit",
    "title": "Pina de Ebro2",
    "text": "Pina de Ebro2 is a town close to Zaragoza and The Ebro River",
    "summary": 'Added some information',
    "format": "json",
    "token": csrf_token
})
create_page_data = create_page_response.json()
print("Create page: " + str(create_page_data))

